#ifndef TCON_H
#define TCON_H

#include <QObject>
#include <QTcpSocket>
#include <QHostAddress>

class tcon : public QObject
{
    Q_OBJECT
public:
    tcon(QTcpSocket* socket, int port, QHostAddress addr, QObject *parent = 0);
    ~tcon();
    
public slots:
    void tserverReadyRead();
    void tserverError(QAbstractSocket::SocketError error);
    void tserverDisconnect();
    void tclientReadyRead();
    void tclientError(QAbstractSocket::SocketError error);
    void tclientDisconnect();

signals:
    void socClose(tcon *obj);

private:
    int opens;
    QTcpSocket* toserver;
    QTcpSocket* tclient;

    void terminate();

};

#endif // TCON_H
