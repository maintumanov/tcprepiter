#include <QCoreApplication>
#include <qcommandlineparser.h>
#include <qcommandlineoption.h>
#include <QTextCodec>
#include <QSettings>
#include "tserver.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QTextCodec * codec  = QTextCodec::codecForName("utf-8");
    // QTextCodec *codec = QTextCodec::codecForName("cp1251");
    QTextCodec::setCodecForLocale(codec);

    QString address = QLatin1String("");
    quint16 tport = 0;
    quint16 sport = 0;
    bool isSave = false;

    QCoreApplication::setOrganizationName("SignalNet");
    QCoreApplication::setApplicationName("TCP Repiter");
    QCoreApplication::setApplicationVersion("0.3");

    QCommandLineParser parser;
    parser.setApplicationDescription("TCP Repiter");
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption targetDirectoryOptionP(QStringList() << "p" << "tport",
                                              QCoreApplication::translate("main", "Sets the connection and connect port."),
                                              QCoreApplication::translate("main", "Port number"));
    parser.addOption(targetDirectoryOptionP);

    QCommandLineOption targetDirectoryOptionI(QStringList() << "i" << "sport",
                                              QCoreApplication::translate("main", "Sets the connection and listening port."),
                                              QCoreApplication::translate("main", "Port number"));
    parser.addOption(targetDirectoryOptionI);

    QCommandLineOption targetDirectoryOptionA(QStringList() << "a" << "address",
                                              QCoreApplication::translate("main", "Sets the connection address."),
                                              QCoreApplication::translate("main", "IP address"));
    parser.addOption(targetDirectoryOptionA);

    QCommandLineOption targetDirectoryOptionS(QStringList() << "s" << "save",
                                              QCoreApplication::translate("main", "Save current settings"));
    parser.addOption(targetDirectoryOptionS);

    parser.process(a);

    if (parser.isSet(targetDirectoryOptionP)) tport = parser.value(targetDirectoryOptionP).toInt();
    if (parser.isSet(targetDirectoryOptionI)) sport = parser.value(targetDirectoryOptionI).toInt();
    else sport = tport;
    if (parser.isSet(targetDirectoryOptionA)) address = QString(parser.value(targetDirectoryOptionA).toLocal8Bit());
    isSave = parser.isSet(targetDirectoryOptionS);

    QSettings settings;
    if (tport == 0 || address == "") {
        tport = settings.value("tport", tport).toInt();
        sport = settings.value("sport", sport).toInt();
        address = settings.value("address", address).toString();
        isSave = false;
    }

    if (tport == 0 || address == "") {
        qDebug() << "no read settings";
        return 0;
    }

    if (isSave) {
        settings.setValue("tport", tport);
        settings.setValue("sport", sport);
        settings.setValue("address", address);
        qDebug() << "saved";
    }
    qDebug() << "Connect to" << address << "port target" << tport << "port source" << sport;

    // tserver serv(3234, QHostAddress("192.168.128.56"));
    tserver serv(sport, tport, QHostAddress(address));

    return a.exec();
}
