#include "tserver.h"

tserver::tserver(quint16 ports, quint16 portt, QHostAddress address, QObject *parent) :
    QObject(parent)
{
    tport = portt;
    sport = ports;
    if (sport == 0) sport = tport;
    saddress = address;
    server = new QTcpServer();
    if (!server->listen(QHostAddress::Any, sport)) {
        qDebug() << "Unable to start the server:" << server->errorString();
        server->close();
        return;
    }
    connect(server, SIGNAL(newConnection()), this, SLOT(slotNewConnection()));
}

void tserver::slotNewConnection()
{
    // QTcpSocket* socket = server->nextPendingConnection();
    tcon *c = new tcon(server->nextPendingConnection(), tport, saddress, this);
    connections.append(c);
    connect(c, SIGNAL(socClose(tcon*)), this, SLOT(socClose(tcon*)));
}

void tserver::socClose(tcon *obj)
{
    for (int i = connections.count() - 1; i >= 0; i --)
        if (connections[i] == obj) {
            connections[i]->deleteLater();
            connections.removeAt(i);
        }
}
