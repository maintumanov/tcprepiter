#ifndef TSERVER_H
#define TSERVER_H

#include <QObject>
#include <QTcpServer>
#include "tcon.h"

class tserver : public QObject
{
    Q_OBJECT
public:
    tserver(quint16 ports, quint16 portt, QHostAddress address, QObject *parent = 0);
    QTcpServer* server;
    
public slots:
    void slotNewConnection();
    void socClose(tcon *obj);

private:
    QList<tcon*> connections;
    quint16 tport;
    quint16 sport;
    QHostAddress saddress;

};

#endif // TSERVER_H
