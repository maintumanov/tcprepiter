#include "tcon.h"

tcon::tcon(QTcpSocket *socket, int port, QHostAddress addr, QObject *parent) :
    QObject(parent)
{
    opens = 1;
    tclient = socket;
    toserver = new QTcpSocket(this);

    connect(toserver, SIGNAL(readyRead()), SLOT(tserverReadyRead()));
    connect(tclient, SIGNAL(disconnected()), SLOT(tserverDisconnect()));
    connect(toserver, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(tserverError(QAbstractSocket::SocketError)));

    connect(tclient, SIGNAL(readyRead()), SLOT(tclientReadyRead()));
    connect(tclient, SIGNAL(disconnected()), SLOT(tclientDisconnect()));
    connect(tclient, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(tclientError(QAbstractSocket::SocketError)));

    toserver->connectToHost(addr, port);
    int connected = toserver->waitForConnected(3000);
    if (!connected) {
        qDebug() << "Connect to server failed";
        tclient->close();
    }
    opens = 2;
}

tcon::~tcon()
{
    delete tclient;
}

void tcon::tserverReadyRead()
{
    if (toserver->bytesAvailable() == 0) return;
    QByteArray ba;
    ba = toserver->read(toserver->bytesAvailable());
    tclient->write(ba);
    toserver->flush();
}

void tcon::tserverError(QAbstractSocket::SocketError )
{
    tclient->close();
    toserver->close();
}

void tcon::tserverDisconnect()
{
    terminate();
}

void tcon::tclientReadyRead()
{
    if (tclient->bytesAvailable() == 0) return;
    QByteArray ba;
    ba = tclient->read(tclient->bytesAvailable());
    toserver->write(ba);
    toserver->flush();
}

void tcon::tclientError(QAbstractSocket::SocketError )
{
    tclient->close();
    toserver->close();
}

void tcon::tclientDisconnect()
{
    terminate();
}

void tcon::terminate()
{
    opens --;
    if (opens > 0) return;
    socClose(this);
}

